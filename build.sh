#!env bash

set -x
set -e 

if [ "$SKIP_TEST" = "true" ]; then
        OPTS="-DskipTests"
fi

#mvn clean -U install -s mvn_settings.xml -gs mvn_settings.xml $OPTS
mvn clean -U package $OPTS
#mvn clean -U compile $OPTS
