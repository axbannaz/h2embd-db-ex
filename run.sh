#!env bash
set -x
java -cp target/embd-db-1.0-SNAPSHOT.jar:${HOME}/.m2/repository/com/h2database/h2/1.4.196/h2-1.4.196.jar \
 com.h2.examples.H2FileDatabaseExample $@
